package com.tranquanganh.intent_truyen_du_lieu_qua_lai;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText edta , edtkq;
    Button btnReQuest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edta = findViewById(R.id.edt_nhapData);
        edtkq = findViewById(R.id.edt_kq);
        btnReQuest = findViewById(R.id.btn_yk);
        // xu ly click
        btnReQuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myintent = new Intent(MainActivity.this,ResultActivity.class);
                int a = Integer.parseInt(edta.getText().toString());
                myintent.putExtra("soA",a);
                startActivityForResult(myintent,19);
            }
        });
    }
}