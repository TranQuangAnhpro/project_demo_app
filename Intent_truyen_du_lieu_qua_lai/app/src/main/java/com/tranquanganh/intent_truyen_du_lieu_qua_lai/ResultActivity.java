package com.tranquanganh.intent_truyen_du_lieu_qua_lai;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ResultActivity extends AppCompatActivity {
    EditText edtAA;
    Button btngoc , btnbp;
    Intent myintent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        edtAA = findViewById(R.id.edt_kq);
        btngoc =  findViewById(R.id.btn_gg);
        btngoc = findViewById(R.id.btn_gbp);
        // nhan itent
        myintent = getIntent();
        //lay intent
        int a = myintent.getIntExtra("soA",0);
        edtAA.setText(a + "");
        btngoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myintent.putExtra("key",a);
                // tra ket qua
                setResult(33,myintent);
                finish();
            }
        });
        btnbp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myintent.putExtra("key",a*a);
                setResult(44,myintent);
                finish();
            }
        });
    }
}